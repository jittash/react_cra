import React from "react";
import "../App.css";

export interface Props{
    button_content:string;
    hover:boolean;
}

const Button:React.FC<Props> = ({button_content,hover}) => {
    return(
        <div className="card-btn">
        <button type="button" className={(hover)?'hover_btn':''}>{button_content}</button>
        </div>
    )
}

export default Button;