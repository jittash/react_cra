import React from "react"
import { Pricing } from "../types";
import "../App.css";
import Button from "./Button";

export interface Props {
    data: Pricing;
}

export interface State {
    hover: boolean;
}

class PricingCard extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            hover: false,
        }
        this.handleHover = this.handleHover.bind(this);
        this.handleLeave = this.handleLeave.bind(this);
    }

    handleHover = () => {
        this.setState((state) => (
            { hover: !state.hover }
        ))
    }

    handleLeave = () => {
        this.setState((state) => (
            { hover: !state.hover }
        ))
    }



    render() {
        const hovered_item = this.state.hover ? 'hover' : '';
        return (
            <li className={`pricingCard ${hovered_item}`} onMouseEnter={this.handleHover} onMouseLeave={this.handleLeave}>
                <div className={`card-title ${this.props.data.title}`} >
                    <span className="star-symbol">&#9734;</span>
                    <strong>{this.props.data.title}</strong><br />
                    {
                        (this.props.data.mostpopular)
                            ?
                            <span className="most-popular">Most popular</span>
                            : null
                    }

                </div>
                <div className="card-body">
                    <div className="card-price">
                        <span className="dollar">${this.props.data.price}</span>/mo
                    </div>
                    <div className="card-content">
                        {this.props.data.limit} users included <br />
                        {this.props.data.storage} GB of storage <br />
                        Help center access <br />
                        {this.props.data.support} support<br />
                    </div>
                    <Button button_content={this.props.data.button} hover={this.state.hover} />

                </div>

            </li>
        )
    }
}

export default PricingCard;