import React from "react";
import "../App.css";

const Header = () => {
    return (
        <div className="header">
            <div className="header-title">Pricing</div>
            <div className="header-description">Quickly build an effective pricing table for your potential customers with this layout. It's built with default Material-UI components with little customization.</div>

        </div>
    )
}

export default Header;