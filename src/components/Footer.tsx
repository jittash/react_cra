import React from "react";
import "../App.css";

const Footer = () => {
    return(
        <div className="footer">
            <ul>
                <li><a href="#"><b>Company</b></a></li>
                <li><a href="#">Team</a></li>
                <li><a href="#">History</a></li>
                <li><a href="#">Contact us</a></li>
                <li><a href="#">Locations</a></li>
            </ul>
            <ul>
                <li><a href="#"><b>Features</b></a></li>
                <li><a href="#">Cool stuff</a></li>
                <li><a href="#">Random feature</a></li>
                <li><a href="#">Team feature</a></li>
                <li><a href="#">Developer stuff</a></li>
                <li><a href="#">Another one</a></li>
            </ul>
            <ul>
                <li><a href="#"><b>Resources</b></a></li>
                <li><a href="#">Resource</a></li>
                <li><a href="#">Resource name</a></li>
                <li><a href="#">Another resource</a></li>
                <li><a href="#">Final resource</a></li>
            </ul>
            <ul>
                <li><a href="#"><b>Legal</b></a></li>
                <li><a href="#">Privacy policy</a></li>
                <li><a href="#">Terms of use</a></li>
            </ul>

        </div>
    )
}

export default Footer;