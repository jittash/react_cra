export interface Pricing {
    id: number;
    title: string;
    price: number;
    limit: number;
    storage: number;
    support: string;
    button: string;
    mostpopular:boolean;
}