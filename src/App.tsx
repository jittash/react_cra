import React from 'react';
import './App.css';
import PricingCard from './components/PricingCard'
import Header from './components/Header'
import Footer from './components/Footer'


function App() {
  const pricingData = [
    {
      id: 1,
      title: 'Free',
      price: 0,
      limit: 10,
      storage: 2,
      support: 'Email',
      button: 'SIGN UP FOR FREE',
      mostpopular: false,
    },
    {
      id: 2,
      title: 'Pro',
      price: 15,
      limit: 20,
      storage: 10,
      support: 'Priority Email',
      button: 'GET STARTED',
      mostpopular: true,
    },
    {
      id: 3,
      title: 'Enterprise',
      price: 30,
      limit: 50,
      storage: 30,
      support: 'Phone & Email',
      button: 'CONTACT US',
      mostpopular: false,
    }
  ]

  return (
    <div className="App">
      <Header />
      <ul className="plans">
        {
          pricingData.map((card) => (
            <PricingCard data={card} key={card.id} />
          ))
        }
      </ul>
      <Footer />
    </div>
  );
}

export default App;
